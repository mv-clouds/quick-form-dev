public with sharing class quickformfieldcomponent1controller {
    
    public class WrapperClass {
        @AuraEnabled
        public list<BaseField__mdt> basefield {get;set;}
        @AuraEnabled
        public list<Page__c> page {get;set;}
        @AuraEnabled
        public list<Form_Field__c> field {get;set;}
        @AuraEnabled
        public String formName {get;set;}
        // public WrapperClass(){
        //     this.basefield = new list<BaseField__mdt>();
        //     this.page = new list<Page__c>();
        //     this.field = new list<Form_Field__c>();
           
        // }
    }      
    @AuraEnabled
    public static WrapperClass fetchQuickFormField(String fId) {
        WrapperClass Wrpcls = new WrapperClass();
        List<BaseField__mdt> basefield = [ SELECT sequence__c,Label,DataRecord__c FROM BaseField__mdt ORDER BY sequence__c];
        List<Page__c> page = [SELECT Id,form__c ,Name,Title__c,Sub_Title__c from Page__c WHERE Form__c =: fId ORDER BY CreatedDate] ;
        List<Form_Field__c> field = [ SELECT Id ,Form__c,Data_Record_Id__c,Page__r.Id FROM Form_Field__c WHERE Page__r.Id IN: page AND  Data_Record_Id__c !=NULL ORDER BY sequence__c];
        List<Form__c> formNameList = [SELECT id ,Title__c FROM Form__c WHERE id =: fId LIMIT 1];
        Wrpcls.basefield = basefield;
        Wrpcls.page = page;
        Wrpcls.field = field;
        Wrpcls.formName = formNameList[0].Title__c;
        return Wrpcls;
    }
    
    @AuraEnabled
    public static WrapperClass addNewPage(String formId) {
        
        Page__c pg = new Page__c();
        pg.Form__c = formId;
        insert pg;

        WrapperClass Wrpcls = fetchQuickFormField(formId);
        return Wrpcls;
    }

    @AuraEnabled
    public static WrapperClass addFieldRecord(String formId,String pageId, String fieldId) {
        
        Form_Field__c formfield = new Form_Field__c();
        formfield.Form__c = formId;
        formfield.Page__c = pageId;
        formfield.Data_Record_Id__c = fieldId;
        insert formfield;
        
        FieldAttribute__c fieldattribute = new FieldAttribute__c();
        fieldattribute.Form_Field__c = formfield.Id;
        insert fieldattribute;

        WrapperClass Wrpcls = fetchQuickFormField(formId);
        return Wrpcls;
    }

    @AuraEnabled
    public static void SequenceSave(List<String> Listt){
        List<Form_Field__c> FormList = new List<Form_Field__c>();
        Set<Id> SetId = new Set<Id>();
        Map<Id,String> MapList = new Map<Id,String>();
        for(String s: Listt){
            List<String> RefStr = s.split(':::');
            SetId.add(RefStr[0]);
            MapList.put(RefStr[0],RefStr[1]+';;'+RefStr[2]);
        }

        FormList = [SELECT Id,Name,Sequence__c,Page__c FROM Form_Field__c WHERE Id=: SetId];
        for(Form_Field__c f: FormList){
            List<String> MapVal = MapList.get(String.valueOf(f.Id)).split(';;');
            f.sequence__c = Decimal.valueOf(MapVal[0]);
            f.Page__c = Id.valueOf(MapVal[1]);
            //f.sequence__c = Decimal.valueOf(MapList.get(String.valueOf(f.Id)));
        }
        update FormList;
    }

    @AuraEnabled
    public static void updateFormName(String fId,String formname){
        List<Form__c> listForm = [SELECT Id ,Title__c from Form__c where Id =: fId];
        for(Form__c form: listForm ){
            form.Title__c = formname;
        }
        update listForm;
        
    }

    @AuraEnabled
    public static void updatePageTitle(String fId,String pageId,String pagetitle){
        List<Page__c> listpagetitle = [SELECT Id ,Form__c,Title__c from Page__c where Id =: pageId ];
        for(Page__c Title: listpagetitle ){
            Title.Title__c = pagetitle;
        }
        update listpagetitle;
        
    }

    @AuraEnabled
    public static void updatePageSubtitle(String fId,String pageId,String pagesubtitle){
        List<Page__c> listpagesubtitle = [SELECT Id ,Form__c,Sub_Title__c from Page__c where Form__c =: fId AND Id =:pageId];
        for(Page__c SubTitle: listpagesubtitle ){
            SubTitle.Sub_Title__c = pagesubtitle;
        }
        update listpagesubtitle;
        
    }
}