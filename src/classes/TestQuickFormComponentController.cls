public with sharing class TestQuickFormComponentController {
    public class WrapperClass {
        @AuraEnabled
        public list<BaseField__mdt> basefield {get;set;}
        @AuraEnabled
        public list<Page__c> page {get;set;}
        @AuraEnabled
        public list<Form_Field__c> field {get;set;}
        @AuraEnabled
        public list<FieldAttribute__c> fieldattribute {get;set;}
    }      
    @AuraEnabled
    public static WrapperClass fetchQuickFormField(String fId) {
        WrapperClass Wrpcls = new WrapperClass();
        List<BaseField__mdt> basefield = [ SELECT sequence__c,Label,DataRecord__c FROM BaseField__mdt ORDER BY sequence__c];
        List<Page__c> page = [SELECT Id,form__c ,Name from Page__c WHERE Form__c =: fId ORDER BY CreatedDate] ;
        List<id> idpage = new List<id>();
        for(Page__c p : page){
            idpage.add(p.Id);
        }
        List<Form_Field__c> field = [ SELECT Id ,Form__c,Data_Record_Id__c,Page__r.Id FROM Form_Field__c WHERE Page__r.Id IN: page AND  Data_Record_Id__c !=NULL ORDER BY sequence__c];

        List<FieldAttribute__c> fieldattribute = [SELECT Id ,Label__c,Visibility__c,Instruction__c,Show_on_click__c,Required_Field__c,Field_Validation__c,Min_no_of_character__c,
                                                Min_no_of_words__c,Total_value__c,Limit_value__c,Show_Decimal__c,Placeholder__c,Placeholder_text__c,
                                                Input_height_in_rows__c,Hide_Field__c,Agreement_Text__c,Read_Only__c,Add_Prefix_Inside_The_Field__c,Prefix__c,Max_no_of_character__c,Max_no_of_words__c,
                                                Date_Format__c,Form_Field__r.Id from FieldAttribute__c where Form_Field__r.Id IN: field ];

        Wrpcls.basefield = basefield;
        Wrpcls.page = page;
        Wrpcls.field = field;
        Wrpcls.fieldattribute = fieldattribute;
        return Wrpcls;
    }
    
    @AuraEnabled
    public static WrapperClass addNewPage(String formId) {
        
        Page__c pg = new Page__c();
        pg.Form__c = formId;
        insert pg;

        WrapperClass Wrpcls = fetchQuickFormField(formId);
        return Wrpcls;
    }

    @AuraEnabled
    public static WrapperClass addFieldRecord(String formId,String pageId, String fieldId) {
        
        Form_Field__c formfield = new Form_Field__c();
        formfield.Form__c = formId;
        formfield.Page__c = pageId;
        formfield.Data_Record_Id__c = fieldId;
        insert formfield;
        
        FieldAttribute__c fieldattribute = new FieldAttribute__c();
        fieldattribute.Form_Field__c = formfield.Id;
        insert fieldattribute;

        WrapperClass Wrpcls = fetchQuickFormField(formId);
        return Wrpcls;
    }

    @AuraEnabled
    public static void SequenceSave(List<String> Listt,String PageId){
        List<Form_Field__c> FormList = new List<Form_Field__c>();
        Set<Id> SetId = new Set<Id>();
        Map<Id,String> MapList = new Map<Id,String>();
        for(String s: Listt){
            List<String> RefStr = s.split(':::');
            SetId.add(RefStr[0]);
            MapList.put(RefStr[0],RefStr[1]+';;'+RefStr[2]);
        }

        FormList = [SELECT Id,Name,Sequence__c,Page__c FROM Form_Field__c WHERE Id=: SetId];
        for(Form_Field__c f: FormList){
            List<String> MapVal = MapList.get(String.valueOf(f.Id)).split(';;');
            f.sequence__c = Decimal.valueOf(MapVal[0]);
            f.Page__c = Id.valueOf(MapVal[1]);
            //f.sequence__c = Decimal.valueOf(MapList.get(String.valueOf(f.Id)));
        }
        update FormList;
    }
}