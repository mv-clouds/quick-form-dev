public with sharing class QuickFormField_Component {
    public class WrapperSave{
        @AuraEnabled
        public FieldAttribute__c AttributeList{get;set;}
        @AuraEnabled
        public List<Field_Value__c> FieldList{get;set;}
        public WrapperSave(){
            this.AttributeList=new FieldAttribute__c();
            this.FieldList=new List<Field_Value__c>();
        }
    }
    @AuraEnabled
    public static WrapperSave saveAttributeList(WrapperSave wlist){
        WrapperSave wrp = new WrapperSave();
        try {            
            update wlist.AttributeList;
            upsert wlist.FieldList;
            wrp.AttributeList = wlist.AttributeList;
            wrp.FieldList = wlist.FieldList;                    
        }catch(Exception e){
        }
        return wrp;
    }    
    public class WrapperClass{
        @AuraEnabled
        public FieldAttribute__c AttributeList{get;set;}
        @AuraEnabled
        public List<Field_Value__c> FieldList{get;set;}
        // @AuraEnabled
        // Public List<Form__c> form{get;set;}
        public WrapperClass(){
            this.AttributeList=new FieldAttribute__c();
            this.FieldList=new List<Field_Value__c>();
            // this.form=new List<Form__c>();
        }
    }
    @AuraEnabled
    public static WrapperClass fetchList(String ids){
        WrapperClass wrpcls=new WrapperClass();
        FieldAttribute__c AttributeList=[ SELECT Id, Name, Label__c, Visibility__c,Field_Validation__c,Total_value__c,Limit_value__c,
                                            Instruction__c, Min_no_of_character__c, Max_no_of_character__c, Hide_Field__c, Form_Field__c, Show_on_click__c, Required_Field__c,
                                            Read_Only__c,Add_Prefix_Inside_The_Field__c,Input_height_in_rows__c,Max_no_of_words__c,Min_no_of_words__c,Prefix__c,Show_Decimal__c, Inline_Choices__c,
                                            Date_Format__c,No_of_columns__c,Agreement_Text__c,Directions__c, Form_Field__r.Data_Record_Id__c,Placeholder__c,Placeholder_text__c
                                            FROM FieldAttribute__c where Form_Field__c =: ids limit 1];
        List<Field_Value__c> FieldList=[ SELECT Id,Name,Form_Field__c,Form__c FROM Field_Value__c where Form_Field__c =:ids ];
        // List<Form__c> form=[ SELECT Id, Name FROM Form__c where id=:ids];
        System.debug(FieldList);
        wrpcls.AttributeList = AttributeList;
        wrpcls.FieldList=FieldList;
        // wrpcls.form=form;
        
        return wrpcls;
    }
    @AuraEnabled
    public static void deletefieldRecord(String fieldId){
        Form_Field__c[] listIds = [SELECT Id ,Name from Form_Field__c where Id =: fieldId];
        delete listIds;  
    }
    @AuraEnabled
    public static void deleteRecord(String field){
        Field_Value__c valuelist= [SELECT Id,Name,Form_Field__c FROM Field_Value__c where Id =: field ];
        delete valuelist;
    }    
}