public with sharing class ObjectMappingController {

    public class WrapperClass {
        @AuraEnabled
        public list<String> SObjectList {get;set;}
        @AuraEnabled
        public list<FieldAttribute__c> fieldattribute {get;set;}
        @AuraEnabled
        public String form {get;set;}
    }

    public class pairWrapper{
        @AuraEnabled public String label{get; set;}
        @AuraEnabled public String apiName{get; set;}
    }

    @AuraEnabled
    public static WrapperClass fetchobjectField(String formId){
        WrapperClass Wrpcls = new WrapperClass();
        List<string> SObjectList = new List<string>();


        for(Schema.SObjectType sObj : schema.getGlobalDescribe().values()){
            if(sObj.getDescribe().isUpdateable() && sObj.getDescribe().isAccessible()){
                sObjectList.add(sObj.getDescribe().getName()+','+sObj.getDescribe().getLabel());
            }
        }
        List<Form_Field__c> field = [SELECT Id,Name,Form__c,Label__c from Form_Field__c where Form__c =: formId];
        List<FieldAttribute__c> fieldattribute = [SELECT Id,Label__c  from FieldAttribute__c where Form_Field__c IN: field];
        List<Form__c> form = [SELECT Id ,Title__c from Form__c where Id =: formId];
        Wrpcls.SObjectList = SObjectList;
        Wrpcls.fieldattribute = fieldattribute;
        Wrpcls.form = form[0].Title__c;
        return Wrpcls;
    }

    @AuraEnabled
    public static List<pairWrapper> getAllFields(String fld){
        List<pairWrapper> lstfieldname = new List<pairWrapper>();
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(fld).getDescribe().fields.getMap();
      
        for (Schema.SObjectField fields: fieldMap.Values()) {  
            if(fields.getDescribe().isAccessible() && fields.getDescribe().isUpdateable()){
                schema.describefieldresult dfield = fields.getDescribe();
                
                pairWrapper field = new pairWrapper();
                
                field.label            = dfield.getLabel();
                field.apiName          = dfield.getName();
                lstfieldname.add(field);
            }
        }        
        return lstfieldname;
    }

    @AuraEnabled
    public static void fieldMapping(String userObj ,String field){        
        Type idarrayType = Type.forName('List<String>');
        List<String> fieldList = (List<String>) JSON.deserialize(field, idarrayType);
        system.debug('Hello');
        system.debug(userObj);
        system.debug(fieldList);    
    }
    
    @AuraEnabled
    public static formWrapper fetchQuickFormField(String formId){

        List<string> SObjectList = new List<string>();

        for(Schema.SObjectType sObj : schema.getGlobalDescribe().values()){
            if(sObj.getDescribe().isUpdateable() && sObj.getDescribe().isAccessible()){
                sObjectList.add(sObj.getDescribe().getName()+','+sObj.getDescribe().getLabel());
            }
        }


        List<Form_Field__c> fieldListId = new List<Form_Field__c>([SELECT Id ,Form__c,Page__c,Data_Record_Id__c,Page__r.Form__c FROM Form_Field__c Where Form__c =: formId AND Data_Record_Id__c != NULL]);
        List<id> idField = new List<id>();
        for(Form_Field__c p : fieldListId){
            idField.add(p.Id);
        }

        List<BaseField__mdt> basefield = [ SELECT sequence__c,Label,DataRecord__c FROM BaseField__mdt ORDER BY sequence__c];        
        Form__c formName = [SELECT id ,Title__c FROM Form__c WHERE id =: formId LIMIT 1];        
        List<PageWrapper> formpage = new List<PageWrapper>();        
        
        Map<Id,FieldAttribute__c> fieldAttribute = new Map<Id,FieldAttribute__c>();
        for(FieldAttribute__c fieldatt : [SELECT Id ,Label__c,Visibility__c,Instruction__c,Show_on_click__c,Required_Field__c,Field_Validation__c,Min_no_of_character__c,
                                    Min_no_of_words__c,Total_value__c,Limit_value__c,Show_Decimal__c,Placeholder__c,Placeholder_text__c,
                                    Input_height_in_rows__c,Hide_Field__c,Agreement_Text__c,Read_Only__c,Add_Prefix_Inside_The_Field__c,Prefix__c,Max_no_of_character__c,Max_no_of_words__c,
                                    Date_Format__c,Form_Field__c FROM FieldAttribute__c WHERE Form_Field__c IN: idField]){            
            fieldAttribute.put(fieldatt.Form_Field__c,fieldatt);         
        }

        Map<Id,List<FieldWrapper>> formPageMap = new Map<Id,List<FieldWrapper>>();        
        for(Form_Field__c formField : [SELECT Id ,Form__c,Page__c,Data_Record_Id__c,Page__r.Form__c FROM Form_Field__c Where Form__c =: formId AND Data_Record_Id__c != NULL ORDER BY Sequence__c]){
            if(formPageMap.containsKey(formField.Page__c)){
                List<FieldWrapper> fList = formPageMap.get(formField.Page__c);
                FieldWrapper fieldwrapper = new FieldWrapper();
                fieldwrapper.FieldObj = formField;
                fieldwrapper.FieldAttObj = fieldAttribute.get(formField.Id);
                fList.add(fieldwrapper);
                formPageMap.put(formField.Page__c,fList);
            }else{
                List<FieldWrapper> fList = new List<FieldWrapper>();
                FieldWrapper fieldwrapper = new FieldWrapper();
                fieldwrapper.FieldObj = formField;
                fieldwrapper.FieldAttObj = fieldAttribute.get(formField.Id);
                fList.add(fieldwrapper);
                formPageMap.put(formField.Page__c,fList);
            }
        }
        for(Page__c p : [SELECT Id,form__c ,Name,Title__c,Sub_Title__c FROM Page__c WHERE Form__c =: formId ORDER BY CreatedDate]){
            PageWrapper page = new PageWrapper();
            page.PageObj = p;
            page.FieldWrapperList = formPageMap.get(p.Id);
            formpage.add(page);
        }      
        formWrapper form = new formWrapper();
        form.formName = formName;
        form.PageWrapperList = formpage;
        form.SObjectList = SObjectList;
        return form;        
    }
    public class formWrapper{
        @AuraEnabled public Form__c formName {get; set;} 
        @AuraEnabled public List<PageWrapper> PageWrapperList {get;set;}
        @AuraEnabled public List<String> SObjectList{get;set;}

    }
    public class PageWrapper{
        @AuraEnabled public Page__c PageObj {get;set;}
        @AuraEnabled public List<FieldWrapper> FieldWrapperList {get;set;}
    }

    public class FieldWrapper{
        @AuraEnabled public Form_Field__c FieldObj {get;set;}
        @AuraEnabled public FieldAttribute__c FieldAttObj {get;set;} 
    }  























    // @AuraEnabled
    // public static void SaveList(String objectApi,String Api){
    //     System.debug('Hello..');
    //     System.debug(objectApi);
    //     Type idarrayType = Type.forName('List<String>');
    //     List<String> fieldList = (List<String>) JSON.deserialize(Api, idarrayType);
    //     // List<Form_Field__c> form= [SELECT Id, Name, API_Name__c FROM Form_Field__c where id=: api];
    //     // System.debug(form);
    //     System.debug(fieldList);
    //     // List<Form_Field__c> Api1=new List<Form_Field__c>();
    //     // List<Form_Field__c> formfield=new List<Form_Field__c>();
    //     // for(Form_Field__c field:formfield){
    //     //     field.Object_API_Name__c=objectApi;
    //     //     // System.debug('objectApi);
    //     //     field.API_Name__c=Api;
    //     //     formfield.add(field);
    //     // }
    //     // insert formfield;
    // }
    
}