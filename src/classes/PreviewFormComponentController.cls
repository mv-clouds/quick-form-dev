public with sharing class PreviewFormComponentController {

    @AuraEnabled
    public static formWrapper getPreviewFormField(String formId){
        List<Form_Field__c> fieldListId = new List<Form_Field__c>([SELECT Id ,Form__c,Page__c,Data_Record_Id__c,Page__r.Form__c,Object_API_Name__c,API_Name__c FROM Form_Field__c Where Form__c =: formId AND Data_Record_Id__c != NULL]);
        List<id> idField = new List<id>();
        for(Form_Field__c p : fieldListId){
            idField.add(p.Id);
        }
      
        Form__c formName = [SELECT id ,Title__c FROM Form__c WHERE id =: formId LIMIT 1];        
        List<PageWrapper> formpage = new List<PageWrapper>();        
        
        Map<Id,FieldAttribute__c> fieldAttribute = new Map<Id,FieldAttribute__c>();
        for(FieldAttribute__c fieldatt : [SELECT Id ,Label__c,Visibility__c,Instruction__c,Show_on_click__c,Required_Field__c,Field_Validation__c,Min_no_of_character__c,
                                    Min_no_of_words__c,Total_value__c,Limit_value__c,Show_Decimal__c,Placeholder__c,Placeholder_text__c,
                                    Input_height_in_rows__c,Hide_Field__c,Agreement_Text__c,Read_Only__c,Add_Prefix_Inside_The_Field__c,Prefix__c,Max_no_of_character__c,Max_no_of_words__c,
                                    Date_Format__c,Form_Field__c FROM FieldAttribute__c WHERE Form_Field__c IN: idField]){            
            fieldAttribute.put(fieldatt.Form_Field__c,fieldatt);         
        }

        Map<Id,List<Field_Value__c>> fieldValue = new Map<Id,List<Field_Value__c>>();
        for(Field_Value__c fieldval : [SELECT Id , Name , Form_Field__c FROM Field_Value__c WHERE Form_Field__c IN: idField]){
            if(fieldValue.containsKey(fieldval.Form_Field__c)){
            List<Field_Value__c> fList = fieldValue.get(fieldval.Form_Field__c);
            fList.add(fieldval);
            fieldValue.put(fieldval.Form_Field__c,fList);
            }else{
                List<Field_Value__c> fList = new List<Field_Value__c>();
                fList.add(fieldval);
                fieldValue.put(fieldval.Form_Field__c,fList);
            }
        }
        Map<Id,List<FieldWrapper>> formPageMap = new Map<Id,List<FieldWrapper>>();        
        for(Form_Field__c formField : [SELECT Id ,Form__c,Page__c,Data_Record_Id__c,Page__r.Form__c FROM Form_Field__c Where Form__c =: formId AND Data_Record_Id__c != NULL ORDER BY Sequence__c]){
            if(formPageMap.containsKey(formField.Page__c)){
                List<FieldWrapper> fList = formPageMap.get(formField.Page__c);
                FieldWrapper fieldwrapper = new FieldWrapper();
                fieldwrapper.FieldObj = formField;
                fieldwrapper.FieldAttObj = fieldAttribute.get(formField.Id);
                fieldwrapper.FieldValueObj = fieldValue.get(formField.Id);
                fList.add(fieldwrapper);
                formPageMap.put(formField.Page__c,fList);
            }else{
                List<FieldWrapper> fList = new List<FieldWrapper>();
                FieldWrapper fieldwrapper = new FieldWrapper();
                fieldwrapper.FieldObj = formField;
                fieldwrapper.FieldAttObj = fieldAttribute.get(formField.Id);
                fieldwrapper.FieldValueObj = fieldValue.get(formField.Id);
                fList.add(fieldwrapper);
                formPageMap.put(formField.Page__c,fList);
            }
        }
        for(Page__c p : [SELECT Id,form__c ,Name,Title__c,Sub_Title__c FROM Page__c WHERE Form__c =: formId ORDER BY CreatedDate]){
            PageWrapper page = new PageWrapper();
            page.PageObj = p;
            page.FieldWrapperList = formPageMap.get(p.Id);
            formpage.add(page);
        }      
        formWrapper form = new formWrapper();
        form.formName = formName;
        form.PageWrapperList = formpage;
        return form;        
    }

    public class formWrapper{
        @AuraEnabled public Form__c formName {get; set;} 
        @AuraEnabled public List<PageWrapper> PageWrapperList {get;set;}
    }
    public class PageWrapper{
        @AuraEnabled public Page__c PageObj {get;set;}
        @AuraEnabled public List<FieldWrapper> FieldWrapperList {get;set;}
    }
    public class FieldWrapper{
        @AuraEnabled public Form_Field__c FieldObj {get;set;}
        @AuraEnabled public FieldAttribute__c FieldAttObj {get;set;}  
        @AuraEnabled public List<Field_Value__c> FieldValueObj {get;set;}
    }    
}