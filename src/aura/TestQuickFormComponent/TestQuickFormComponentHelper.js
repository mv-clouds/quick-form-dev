({
    onaddpage : function(component,event,helper,formId){
        component.set("v.spinner", true);
        var action = component.get('c.addNewPage');
        action.setParams({'formId': formId});
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {  
                component.set("v.FormPageFieldWrapper", response.getReturnValue());
                component.set("v.spinner", false);                
            }
            else{
                component.set("v.spinner", false);
            }
        });
        $A.enqueueAction(action);                  
	},
    insertFieldRecord : function(component,event,helper,FormId,PageId,Fieldid){
        component.set("v.spinner", true);
        var action = component.get('c.addFieldRecord');
        action.setParams({'formId': FormId , 'pageId' : PageId ,'fieldId' : Fieldid});
        action.setCallback(this,function(response){   
            component.set("v.spinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {  
                component.set("v.FormPageFieldWrapper", response.getReturnValue());
                component.set("v.spinner", false);                
            }
            else{
                component.set("v.spinner", false);
            }
        });   
        $A.enqueueAction(action);
    },    
    SequenceSave: function(component,event,helper,Listt){
        var action = component.get("c.SequenceSave");
        action.setParams({
            'Listt' : Listt 
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            var res = response.getReturnValue();
            if (state === "SUCCESS") {
            }else{
            }
        });
        $A.enqueueAction(action);
    }
})