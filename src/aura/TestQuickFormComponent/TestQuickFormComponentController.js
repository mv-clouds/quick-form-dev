({
    init : function(component,event,helper){
        var list = component.get('v.quickformfieldcomponent1');
        component.set('v.quickformfieldcomponent1',list);
    },
    openDesignComponent : function(component, event, helper) {
        var btnclick = event.getSource();
        var title = btnclick.get("v.title");
        component.set("v.label",true);
    },
    openFormBuildComponent : function(component, event, helper) {
        var btnclick1 = event.getSource();
        var title1 = btnclick1.get("v.title");
        component.set("v.label",false);
    },
    onDragStart:function(form,event,helper){
        var x = event.target.dataset.record;          
        event.dataTransfer.setData('text/plain',x);       
    },
    onDragOver : function(from,event){
        event.preventDefault();
    },
    onDrop : function(component,event,helper,form){
        const Fieldid = event.dataTransfer.getData('text');
        var xc = document.querySelector('[data-record="'+Fieldid+'"]');
        var dataRef = xc.getAttribute('data-ref');
        if(dataRef == "inner"){
            event.target.appendChild(xc);
            var classname = event.target.className; 
            if(classname == 'field'){
                var FieldElement =  document.querySelectorAll('.field');
                var Listt = [];
                for(var i =0;i<FieldElement.length;i++){
                    var x = FieldElement[i].getAttribute('data-record');
                    var ParentPageId = FieldElement[i].getAttribute('id');
                    Listt.push(x+':::'+i+':::'+ParentPageId);
                }  
                helper.SequenceSave(component,event,helper,Listt);
                            
            }else if(classname == 'example-dropzone'){
                var FieldElement =  document.querySelectorAll('.field');
                var Listt = [];
                for(var i =0;i<FieldElement.length;i++){
                    var x = FieldElement[i].getAttribute('data-record');
                    var ParentPageId = FieldElement[i].parentElement.parentElement.getAttribute('id');                
                    Listt.push(x+':::'+i+':::'+ParentPageId);
                }           
                helper.SequenceSave(component,event,helper,Listt);             
            }             
        }else{
            var FormId = component.get("v.FormId");                        
            var classname = event.target.className;
            if(classname == 'field'){
                var PageId = event.target.parentNode.parentNode.id;                
            }else if(classname == 'example-dropzone'){
                var PageId = event.target.parentNode.id;                        
            }    
            var CloneObject = xc.cloneNode(true);        
            event.target.appendChild(CloneObject);
            helper.insertFieldRecord(component,event,helper,FormId,PageId,Fieldid);
        }
        event.dataTransfer.cleardata();
    },
    fetchQuickField : function(component, event, helper){
        var FId = component.get("v.FormId");
        var action = component.get("c.fetchQuickFormField");
        action.setParams({'fId' : FId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {            
                component.set("v.FormPageFieldWrapper", response.getReturnValue());                          
            }
        });
        $A.enqueueAction(action);
    },
    onaddpage : function(component,event,helper){
        var formId = component.get("v.FormId");      
        helper.onaddpage(component,event,helper,formId); 
    },
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event,helper){  
        component.set("v.spinner", false);
    },
    handleid : function(component, event, helper){
        var target=event.target.name;
        var evt = $A.get("e.c:FieldEvent");
        evt.setParams({ "records": target});
        evt.fire(); 
    },
    handleSampleEvent : function(component, event, helper){
        var message=event.getParam("records");
        component.set("v.record",message);
    }
})