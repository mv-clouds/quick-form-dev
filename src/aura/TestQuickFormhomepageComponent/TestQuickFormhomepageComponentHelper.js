({
    getFormList: function(component) {
        
    var action = component.get('c.getForm');
    var self = this;
    action.setCallback(this, function(actionResult) {
    component.set("v.listOfForm", actionResult.getReturnValue());
    });
    $A.enqueueAction(action);
    },
    ondelete : function(component,event,helper,Id) {
        var action = component.get("c.deleterecord"); 
        action.setParams({Id1: Id});
        action.setCallback(this, function(response) {
        component.set("v.listOfForm",response.getReturnValue());
        });
        $A.enqueueAction(action);
        },

        createForm: function(component, event, helper) {
        //console.log("From Helper");
        var formId = component.get("v.Form");
        //contact.Name = component.get('v.recordId');
        // Initializing the toast event to show toast
        var toastEvent = $A.get('e.force:showToast');
        var createAction = component.get('c.createFormrecord');
        
        createAction.setParams({ 'formId' : formId });
        createAction.setCallback(this, function(response) {           
            var state = response.getState();
            //console.log({state});
            //var x =  response.getReturnValue();
            //console.log({x});
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
            componentDef: "c:QuickFormComponent",
            componentAttributes :{
                FormId : response.getReturnValue()
            }
        });
         evt.fire();
        });
        //var x =  response.getReturnValue("v.newForm.Id");
        //console.log({x});
        $A.enqueueAction(createAction);
    },
    openModal : function(component, event, helper) {
        var modal = component.find("Modal");
        var modalBackdrop = component.find("ModalBackdrop");
        $A.util.addClass(modal,"slds-fade-in-open");
        $A.util.addClass(modalBackdrop,"slds-backdrop_open");
    },
    closeModal : function(component, event, helper) {
        var modal = component.find("Modal");
        var modalBackdrop = component.find("ModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    }
})