({
    
    openModal: function(component, event, helper) {
        helper.openModal(component, event, helper);        
    },

    closeModal: function(component, event, helper) {
        helper.closeModal(component, event, helper);
    },

    createForm: function(component, event, helper) {
        helper.createForm(component, event, helper);
    },

    doInit : function(component, event, helper) {
        helper.getFormList(component); 
    },
    ondelete : function(component, event, helper) {
        if(confirm('Are you sure?'))
        var Id = event.getSource().get('v.name');
        helper.ondelete(component,event,helper,Id);
        helper.getFormList(component); 
    },
    onedit : function(component, event, helper) {
        var msg ='Are you sure you want to edit this form?';
        if(!confirm(msg)){
            return false;        }
        else {
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef: "c:QuickFormComponent",
                componentAttributes :{
                    FormId : event.getSource().get("v.name") 
            }
        });
        evt.fire();
        }
    } ,
    onpreview : function(component, event, helper) {
        var msg ='Are you sure you want to preview this form?';
        if(!confirm(msg)){
            return false;
        }
        else {
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef: "c:PreviewFormComponent",
                componentAttributes :{
                    FormId : event.getSource().get("v.name") 
            }
        });
        evt.fire();
        }
    } 
})




//1 
// doInit : function(component, event, helper) {  event.getSource().get("v.Id")
//     helper.getFormList(component); 
// },
// onCreateForm : function(component, event, helper) {
//     var evt = $A.get("e.force:navigateToComponent");
//     evt.setParams({
//         componentDef: "c:QuickFormComponent",
//         componentAttributes :{
//         //your attribute
//         }
//     });
   
// evt.fire();
// },
// ondelete : function(component, event, helper) {
//     var accountList = component.get("v.lstForm1");
//     //console.log(accountList);
//     // var selectedItem = event.currentTarget;//getSource().get('v.n1');
//     var selectedItem = event.getSource().get('v.title');
//     console.log({selectedItem});
//     var index = selectedItem.dataset.record;
//     console.log(index);
//     accountList.splice(index, 1);
//     component.set("v.lstForm1", accountList);

// },
// onedit : function(component, event, helper) {
//     alert("edit?");
// } 


//2
// addRow: function(component, event, helper) {
//     get the account List from component  
//     var formList = component.get("v.formList");
//     Add New Account Record
//     formList.push({
//         'sobjectType': 'Form__c',
//         'Title__c': '', 
        
//     });
//     component.set("v.formList", formList);
// },

// removeRecord: function(component, event, helper) {
//     var formList = component.get("v.formList");
//     var selectedItem = event.currentTarget;
//     var index = selectedItem.dataset.record;
//     formList.splice(index, 1);
//     component.set("v.formList", formList);
// },
    
// saveForm: function(component, event, helper) {      
//     if (helper.validateAccountRecords(component, event)) {
//         Call Apex method and pass account list as a parameters
//         var action = component.get("c.saveFormList");
//         action.setParams({
//             "formList": component.get("v.formList")
//         });
//         action.setCallback(this, function(response) {
//             get response status 
//             var state = response.getState();
//             if (state === "SUCCESS") {
//                 set empty account list
//                 component.set("v.formList", []);
//                 alert('Accounts saved successfully');
//             }
//         }); 
//         var evt = $A.get("e.force:navigateToComponent");
//         evt.setParams({
//             componentDef: "c:QuickFormComponent",
//             componentAttributes :{
//             your attribute
//             }
//         });

//          evt.fire();
//         $A.enqueueAction(action);
//     }
// },



//3 juni helper method
// var formList = component.get("v.lstForm");
        // //console.log(formList);
        // // var selectedItem = event.currentTarget;//getSource().get('v.n1');
        // var selectedItem = event.getSource().get('v.title');
        // console.log({selectedItem});
        // var index = selectedItem.dataset.record;
        // console.log(index);
        // formList.splice(index, 1);
        // component.set("v.lstForm", formList);