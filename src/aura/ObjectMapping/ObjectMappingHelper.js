({
    // getfieldsHelper: function(component,event,helper,userObj){
    //     var action = component.get("c.getAllFields");
    //     action.setParams({
    //         "fld": userObj
    //     });
    //     var opts=[];
    //     action.setCallback(this, function(response) {
    //         var state = response.getState();
    //         //console.log({state});
    //         //console.log(response.getReturnValue());
    //         if (state == "SUCCESS") {
    //            var allValue = response.getReturnValue();               
    //            console.log({allValue});
    //            component.set("v.allValue", response.getReturnValue());
    //         //     for (var i = 0; i < allValue.length; i++) {
    //         //         opts.push({
    //         //             class: "optionClass",
    //         //             label: allValue[i],
    //         //             value: allValue[i]
    //         //         });
    //         // }
    //         //      component.find("FieldsList").set("v.options", opts);
    //         }
    //         else {
    //             console.log("Failed with state: " + state);
    //         }
    //     });
    //     $A.enqueueAction(action);
    // }
    selectedfieldmap : function(component,event,helper,x){
        var userObj=component.find("SobjectList").get("v.value");
        // console.log({userObj});
        var x = component.get("v.fieldMapList");
        // console.log({x});

        var fields = [];

        for(var i=0;i<x.length;i++){
            fields.push(x[i].fieldId,x[i].fieldApiName);
        }
        // console.log({fields});

        var action = component.get("c.fieldMapping");
        var IdListJSON=JSON.stringify(fields);
        action.setParams({ 'userObj' : userObj , 'field' : IdListJSON});
        action.setCallback(this,function(response){
            var state = response.getState();
            // console.log({state});
            if (state == "SUCCESS") {   
                console.log("Completed");
            }
            else{
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

    fetchObjectField : function(component,event,helper){
        var formId = component.get("v.FormId");
        // console.log({formId});
        var action = component.get("c.fetchQuickFormField");
        action.setParams({
            'formId':formId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            // console.log({state});
            if (state == "SUCCESS"){
                var allvalue=response.getReturnValue();
                //  console.log({allvalue});
                component.set("v.allValues",response.getReturnValue());
                var x = response.getReturnValue().SObjectList;
                var opts = [];
                    for (var i = 0; i < x.length; i++) {
                    opts.push({
                        value : x[i].split(',')[0],
                        label : x[i].split(',')[1]
                    });                    
                    component.set("v.allObject",opts);
                }
            }            
        });
        $A.enqueueAction(action);
    }
})