({
    fetchObjectField: function(component, event, helper) {
        helper.fetchObjectField(component,event,helper);
        //alert(component.get("v.FormId"));
        // var formId = component.get("v.FormId");
        // console.log({formId});
        
        // var action = component.get("c.fetchQuickFormField");
        // var opts=[];
        // action.setParams({'formId' : formId});
        // action.setCallback(this, function(response) {
        //     var state = response.getState();
        //     console.log({state});
        //     if (state == "SUCCESS") {
        //         var arr = [];
        //         var storeResponse = response.getReturnValue().SObjectList;
                
        //         storeResponse.sort();
        //         for(var i=0; i <storeResponse.length; i++){
        //             arr.push({
        //                 value : storeResponse[i].split(',')[0],
        //                 label : storeResponse[i].split(',')[1]
        //             });
        //         }       
        //     component.set("v.allObject", arr);
        //     component.set("v.allValues", response.getReturnValue());
        //     var formfields = response.getReturnValue();
        //     console.log(formfields);
        //     component.set("v.formfields",formfields);
           
        //     var primaryformfields = response.getReturnValue().fields;
        //      console.log({primaryformfields});
        //     component.set("v.primaryformfields",primaryformfields);
        //     } 
        //     else {
        //         console.log("Failed with state: " + state);
        //     }
        // });
        // $A.enqueueAction(action);
    },

    getfields: function(component, event, helper) {
        var userObj=component.find("SobjectList").get("v.value");
        // console.log(userObj);
        // if(userObj == 'Select Object'){
        //     component.set("v.matchField",false);
        // }
        // else{
        //     component.set("v.matchField",true);
        // }
        var action = component.get("c.getAllFields");
        action.setParams({
            "fld": userObj
        });
        var opts=[];
        action.setCallback(this, function(response) {
            var state = response.getState();
            // console.log({state});
            if (state == "SUCCESS") {                
                
                component.set("v.allFields", response.getReturnValue());
                var x = component.get("v.fieldMapList");
                x.push({
                    "fieldId" : "",
                    "fieldApiName" : ""
                });
                component.set("v.fieldMapList",x);                
            }            
            else {
                console.log("Failed with state: " + state);
            }
            var primaryallfields = response.getReturnValue();
            // console.log(primaryallfields);
            component.set("v.primaryallfields",primaryallfields);
        });
        $A.enqueueAction(action);
    },
    // addField :function(component, event, helper) {
    //     var x = component.get("v.fieldMapList");
    //     // console.log(x);
    //     x.push({
    //         "fieldId" : "",
    //         "fieldApiName" : ""
    //     });
    //     component.set("v.fieldMapList",x);                
    // },

    deleteRow: function(component, event, helper) {
        var x = component.get("v.fieldMapList");
        var index = event.getSource().get('v.name');
        x.splice(index,1);
        component.set("v.fieldMapList",x);
    },
    // selectedformfield : function(component, event, helper) {       
    //     var selectedfieldId = event.getSource().get('v.value');  
    //     console.log({selectedfieldId});  
    //     // selectedfieldId.remove();  
    //     // component.set("v.formfields",selectedfieldId).remove();                     
    // },

    selectedsalfield : function(component, event, helper) {
        //var selectedfield = event.getSource().get('v.value');
        //console.log({selectedfield});
        //var userObj=component.find("SobjectList").get("v.value");
        //console.log({userObj});
        //var x = component.get("v.fieldMapList");
        //console.log({x});
        helper.selectedfieldmap(component, event, helper);
        
        
    },
    // savevalue:function(component,event,helper){
    //     var id=component.get("v.fieldMapList");
    //     console.log({id});
    //     var userObj=component.find("SobjectList").get("v.value");
    //     console.log(userObj);
    //     var fields=[];
    //     var IdListJSON=JSON.stringify(fields);
    //     var action=component.get("c.SaveList");
    //     action.setParams({
    //         'objectApi': userObj,
    //         'Api':IdListJSON
    //     });
    //     action.setCallback(this, function(response) {
    //         var state = response.getState();
    //         console.log({state});
    //         // if (state == "SUCCESS") {
    //         // }
    //     });
    //     $A.enqueueAction(action);
    // }
})