({
    getAttributeList :function(component,event,helper){
        var ShowResultValue = event.getParam("records");
        component.set("v.fieldId",ShowResultValue);                                             
        var action=component.get("c.fetchList");
        action.setParams({
            'ids' : ShowResultValue 
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var WrapperValue = response.getReturnValue();
                component.set("v.WrapperTest", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    saveList : function(component,event,helper){
        var id=component.get("v.WrapperTest");
        console.log({id});
        var action=component.get("c.saveAttributeList");
        action.setParams({
            wlist : id
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            //console.log({state});
            if(state === 'SUCCESS'){
                var dataMap =response.getReturnValue();
                //console.log({dataMap});
                component.set("v.WrapperTest",dataMap);
                var x=component.get("v.WrapperTest");
                console.log({x});
            }
        });
        $A.enqueueAction(action);
    },
    deleteRecord :function(component,event,helper){
        component.set("v.spinner", true);
        var fieldId = component.get("v.WrapperTest.AttributeList.Form_Field__c");
        var action = component.get("c.deletefieldRecord");
        action.setParams({ 'fieldId' : fieldId });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
                component.set("v.spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    handlefieldDelete : function(component,event,helper){
        component.set("v.spinner",true);
        var val=event.getSource().get("v.name");
        var action=component.get("c.deleteRecord");
        action.setParams({ 'field' : val });
        action.setCallback(this,function(response){
            var state = response.getState();
            component.set("v.spinner", false);     
        });
        $A.enqueueAction(action);
    },
})