({
    FetchAttributeList:function(component, event, helper){  
        helper.getAttributeList(component,event,helper);
    },
    addvalue :function(component, event, helper){
        var lst=component.get("v.WrapperTest.FieldList");
        console.log({lst});
        var xy = [];
        for(var x of lst){
            xy.push(x.Form__c);
        }
        lst.push({"Name" : "",Form_Field__c:component.get("v.fieldId"),Form__c:xy[0]});
        component.set("v.WrapperTest.FieldList",lst);
    },
    saveValue :function(component, event, helper){
        helper.saveList(component,event,helper);
    },     
    deleteFieldRecord : function(component,event,helper){
        helper.deleteRecord(component,event,helper);
    },
        
    // OPTION 2 FOR DELETE
    // deleteFieldRecord : function(component,event,helper){
    //     component.set("v.spinner", true);
    //     var fieldId = component.get("v.WrapperTest");
    //     console.log(fieldId);
    //     var action = component.get("c.deleteAttributeList");
    //     action.setParams({ 'wlist' : fieldId });
    //     action.setCallback(this,function(response){
    //         var state = response.getState();
    //         if(state === 'SUCCESS'){
    //             $A.get('e.force:refreshView').fire();
    //             component.set("v.spinner", false);
    //         }
    //     });
    //     $A.enqueueAction(action);
    // },

    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event,helper){  
        component.set("v.spinner", false);
    },
    handledelete : function(component, event, helper){
        helper.handlefieldDelete(component,event,helper)
    }, 
    // validation : function(component, event, helper) {
    //     var max = event.getSource().get("v.value");
    //     console.log(max);
    // }
    // handleError : function(component,event, helper){
    //     var inputField=component.find('inputField');
    //     console.log(inputField);
    //     var value=inputField.get('v.value');
    //     console.log(value);
    //     if(value < 'v.WrapperTest.AttributeList.Max_no_of_character__c'){
    //         inputField.set('v.validity', {valid:false, badInput :true});
    //         inputField.showHelpMessageIfInvalid();
    //     }
    // }  
      
    // handleError : function(component,event,helper){
    //     var val=component.find("minval").get("v.value");
    //     if(val > "v.WrapperTest.AttributeList.Max_no_of_character__c"){
    //         val.setCustomValidity("Not more than");
    //     }
    // }
})