({
    fetchQuickFormFieldAttValue : function(component,event,helper){
        component.set("v.spinner", true);
        var formId = component.get("v.FormId");
        var action = component.get("c.fetchQuickFormField");
        action.setParams({'formId' : formId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {            
                var x = response.getReturnValue(); 
                console.log({x});
                component.set("v.FormPageFieldValueWrapper", response.getReturnValue());          
                component.set("v.spinner", false);                       
            }else{
                component.set("v.spinner", false);  
                helper.showToast("Error","Error Occur","Something went wrong to fetch data");
            }
        });
        $A.enqueueAction(action);
    },
    formtitle : function(component,event,helper){
        var formname = event.getSource().get('v.value');
        var formId = component.get("v.FormId");
        var action = component.get("c.updateFormName");
        action.setParams({'formId' : formId , 'formname' : formname});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.editFormTitle",false);
            }else{
                helper.showToast("Error","Error Occur","Something went wrong to update form title");
            }
        });
        $A.enqueueAction(action);
    },
    pagetitle : function(component,event,helper){
        var pagetitle = event.getSource().get('v.value');
        var pageId = event.target.parentNode.parentNode.id;
        var formId = component.get("v.FormId");  
        var action = component.get("c.updatePageTitle");
        action.setParams({'formId' : formId ,'pageId' : pageId, 'pagetitle' : pagetitle});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.editPageTitle",false);
            }else{
                helper.showToast("Error","Error Occur","Something went wrong to update page title");
            }
        });
        $A.enqueueAction(action);
    },
    pagesubtitle: function(component,event,helper){
        var pagesubtitle = event.getSource().get('v.value');
        var pageId = event.target.parentNode.parentNode.id;
        var formId = component.get("v.FormId");
        var action = component.get("c.updatePageSubtitle");
        action.setParams({'formId' : formId ,'pageId' : pageId, 'pagesubtitle' : pagesubtitle});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.editPageSubTitle",false);
            }else{
                helper.showToast("Error","Error Occur","Something went wrong to update page sub title");
            }
        });
        $A.enqueueAction(action);
    },
    onaddpage : function(component,event,helper,formId){
        component.set("v.spinner", true);
        var formId = component.get("v.FormId");  
        var action = component.get('c.addNewPage');
        action.setParams({'formId': formId});
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {  
                component.set("v.FormPageFieldValueWrapper", response.getReturnValue());
                component.set("v.spinner", false);                
            }else{ 
                component.set("v.spinner", false);
                helper.showToast("Error","Error Occur","Something went wrong to add page");
            }
        });
        $A.enqueueAction(action);                  
	},
    deletepage :function(component,event,helper){
        component.set("v.spinner", true);
        var pageId = event.getSource().get('v.name');
        var formId = component.get("v.FormId");  
        var action = component.get("c.DeletePage");
        action.setParams({'pageId' : pageId , 'formId' : formId});
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.FormPageFieldValueWrapper", response.getReturnValue());
                component.set("v.spinner", false);
            }else{
                component.set("v.spinner", false);
                helper.showToast("Error","Error Occur","Something went wrong to delete page");                
            }
        });
        $A.enqueueAction(action);
    },
    insertFieldRecord : function(component,event,helper,FormId,PageId,Fieldid){
        component.set("v.spinner", true);
        var action = component.get('c.addFieldRecord');
        action.setParams({'formId': FormId , 'pageId' : PageId ,'fieldId' : Fieldid});
        action.setCallback(this,function(response){   
            component.set("v.spinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {  
                component.set("v.FormPageFieldValueWrapper", response.getReturnValue());
                component.set("v.spinner", false);                
            }
            else{
                component.set("v.spinner", false);
                helper.showToast("Error","Error Occur","Something went wrong to insert field");
            }
        });   
        $A.enqueueAction(action);
    },    
    SequenceSave: function(component,event,helper,Listt){
        var action = component.get("c.SequenceSave");
        action.setParams({
            'Listt' : Listt 
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            var res = response.getReturnValue();
            if (state === "SUCCESS") {
                //console.log("Complete");
            }else{
                //console.log("Error");
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(type,title,message){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({     
            "title": title,     
            "type": type,     
            "message": message   
        });   
        toastEvent.fire();                
    }
})