({
    fetchPreviewFormField : function(component, event, helper) {    
        helper.fetchPreviewFormField(component, event, helper);
    },
    onNext: function(component, event, helper) {
        helper.onNext(component, event, helper);
    },
    onPrevious: function(component, event, helper) {
        helper.onPrevious(component, event, helper);
    },
    onSubmit: function(component, event, helper) {
        helper.onSubmit(component, event, helper);
    },
})